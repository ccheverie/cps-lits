 �i]<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"178";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-01-28 15:53:19";s:13:"post_date_gmt";s:19:"2019-01-28 22:53:19";s:12:"post_content";s:2259:"<!-- wp:paragraph -->
<p>Once the respondent is served a protection
order, they are required to obey all conditions.&nbsp; If they do not obey a condition, they have
breached the order. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Depending on the conditions of your order,
this may mean any contact with you directly or indirectly is considered a
breach.&nbsp; This includes a text message,
email, phone call, private message via social media or anything like that;
regardless of what the message says or why they are contacting you. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Being within the “do not attend, enter or
be within” distance of you or the addresses outlined in condition conditions is
also a breach. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>When you attend the review date for your
protection order, the respondent will be within the no contact distance.&nbsp; However, the respondent is not allowed to
speak to you, approach you or sit with you in or out of the court room.&nbsp; The court house is not a free zone. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>What
do I do if I suspect the respondent has breached the conditions of the order?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>If the matter is an emergency call
9-1-1.&nbsp; If the matter is not an
emergency, call the non-emergency line of your local police agency. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>The
order says the respondent cannot contact me, can I still contact them?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No.&nbsp;
Do not have any contact with the respondent until your case is settled
through court unless the order specifically allows contact in certain
instances. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Why
is it important to report all breaches to police?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A protection order is a valuable tool that
allows police additional powers to keep you and your family safe.&nbsp; Do not downplay the seriousness of a
breach.&nbsp; Reporting all incidents to
police helps officers enforce orders to prevent more breaches from occurring.&nbsp; </p>
<!-- /wp:paragraph -->";s:10:"post_title";s:39:"What is a breach of a Protection Order?";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:38:"what-is-a-breach-of-a-protection-order";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-02-13 14:33:17";s:17:"post_modified_gmt";s:19:"2019-02-13 21:33:17";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:23:"https://cps-lits/?p=178";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}