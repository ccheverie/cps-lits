��d\<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:256;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-02-13 16:17:50";s:13:"post_date_gmt";s:19:"2019-02-13 23:17:50";s:12:"post_content";s:1464:"<!-- wp:paragraph -->
<p>“Common-law” relationship is defined
differently in Alberta than the rest of Canada.&nbsp;
An Adult Interdependent Relationship is not a marriage.&nbsp; If you’re married, you cannot be in an Adult
Interdependent Relationship.&nbsp; Parties
must meet the following criteria:</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Two unmarried parties living
together for a continuous period of at least three years in which they share
each other’s lives, emotionally committed to each other and function as a
domestic and economic unit;</li><li>Or the unmarried parties have
lived in a relationship of some permanence and have a child of the relationship
by birth or adoption and in which they share each other’s lives, emotionally
committed to each other and function as a domestic and economic unit; or</li><li>The unmarried parties have
entered into an adult interdependent relationship agreement with each other. </li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>The
relationship does not have to be sexual in nature.&nbsp; Therefore, family members who meet the above
definitions may be in an adult interdependent relationship.&nbsp; </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>There are multiple restrictions and guidelines to this act; refer to the act for more information at http://www.qp.alberta.ca/documents/Acts/A04P5.pdf.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:38:"Adult Interdependent Relationships Act";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"inherit";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:15:"255-revision-v1";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-02-13 16:17:50";s:17:"post_modified_gmt";s:19:"2019-02-13 23:17:50";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:255;s:4:"guid";s:33:"https://cps-lits/255-revision-v1/";s:10:"menu_order";i:0;s:9:"post_type";s:8:"revision";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}