 �i]<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"229";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-02-13 14:50:45";s:13:"post_date_gmt";s:19:"2019-02-13 21:50:45";s:12:"post_content";s:5968:"<!-- wp:paragraph -->
<p>Domestic
abuse occurs when one person in an intimate relationship or marriage tries to
dominate and control the other person. Domestic abuse that includes physical
violence is called domestic violence. It is important to remember that domestic
violence does not discriminate against age, race, ethnicity, sexual orientation
or economic status.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Physical</strong> violence includes pushing, shoving, slapping, kicking, punching,
hitting, spitting, pulling hair, choking/strangling, throwing objects, hitting
a victim with an object, using or threatening to use a weapon, physically
restraining someone or preventing someone from leaving such as forced
confinement.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Sexual
Assault </strong>is any unwanted
or forced sexual act or behaviour without your informed consent.&nbsp; </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Consent means actively agreeing to be
sexual with someone.&nbsp; Both people must
agree to sex, every single time, for it to be consensual.&nbsp; Consent may be withdrawn at any point and cannot
be given when intoxicated or unconscious. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Being forced or expected to engage in sex
because of an already existing relationship (such as marriage) is not
consent.&nbsp; Being forced to engage in sex
due to fear of repercussions (withholding support, partners anger etc.) of not
having sexual intimacy is not consent.&nbsp; </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Verbal</strong> abuse is when an abuser directs negative statements and langue
toward someone that causes emotional harm.&nbsp;
It is used to undermine a person’s dignity.&nbsp; It can include accusing and blaming, name
calling, put downs, criticizing and judging, abusive anger, threats, and
minimizing of the other person’s experiences.&nbsp;&nbsp;
</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Financial</strong> Abuse occurs when you are not allowed to have money or any control
over money.&nbsp; This could include running
up large debts in your name or selling your possessions without your
permission. &nbsp;Your partner may keep you
accountable for any money spent, approving or disapproving of your
spending.&nbsp; It could also mean you are not
allowed to have a job so you are dependent on your partner for money and
survival.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Isolation</strong> occurs when you are isolated from your family, friends, and
community as a way for your partner to stay in control.&nbsp; Your partner may be extremely jealous of any
contacts you have, forbid you to have contact with anyone or monitor your phone
calls, mail or daily activities.&nbsp; Sometimes
your partner may use intimidation or threats to control you.&nbsp; You may have to be accountable for your time
away or have to make excuses for leaving the home.&nbsp; You may have to communicate secretly when
your partner is absent. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Emotional/psychological </strong>abuse can cause anxiety and depression
and cause you to withdraw from everyone or everything around you.&nbsp; Examples of this type of abuse include
insulting your family and friends, ridiculing your beliefs, race or religion,
using constant put downs, threatening suicide if you leave, keep you prisoner
in your home, threatening to take children if you leave and threatening to have
you deported.&nbsp; “Gaslighting” is a phrase
used to describe persistent and manipulative brainwashing that causes a victim
to doubt themselves and lose their own sense of perception, identity and
self-worth.&nbsp; There are 7 characteristics
of gaslighting:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>1. Lie and
Exaggerate. The gaslighter creates a negative narrative about the gaslightee
(“There’s something wrong and inadequate about you”). </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>2. Repetition.
Lies are repeated often by the abuser to control and dominate the
relationship.&nbsp; </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>3. Escalation
and Denial.&nbsp; The abuser sows doubt in the
mind of the victim through denial of the abuse. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>4. Wear down the
Victim. Through the constant denial of the victims experience from the abuser,
the victim begins to question their own perception, identity, and reality,
becoming worn out and exhausted. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>5.&nbsp; Codependency.&nbsp;
A codependent relationship is formed based on fear, vulnerability, and
marginalization. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>6. False Hope.
This is often referred to during the spiral of abuse as the honeymoon phase.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Spiritual
abuse</strong> is when an abuser engages in the denial
or use of spiritual or religious beliefs and practices to control and dominate.
Spiritual abuse damages an individual’s spiritual experiences.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Coercive
Control</strong> is an act or a pattern of acts of
assault, threats, humiliation and intimidation or other abuse that is used to
harm, punish, or frighten their victim.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>This controlling behaviour is designed to
make a person dependent by isolating them from support, exploiting them,
depriving them of independence and regulating their everyday behaviour.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Coercive control creates invisible chains
and a sense of fear that pervades all elements of a victim’s life. It works to
limit their human rights by depriving them of their liberty and reducing their
ability for action.</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:27:"What Are Types of Violence?";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:26:"what-are-types-of-violence";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-02-13 16:13:01";s:17:"post_modified_gmt";s:19:"2019-02-13 23:13:01";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:23:"https://cps-lits/?p=229";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}