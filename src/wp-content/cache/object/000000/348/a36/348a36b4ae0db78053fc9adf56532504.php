�d\<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:269;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-02-13 16:53:21";s:13:"post_date_gmt";s:19:"2019-02-13 23:53:21";s:12:"post_content";s:2943:"<!-- wp:heading {"level":3} -->
<h3>​In an emergency situation, please call 9-1-1 immediately.
&nbsp;</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><strong>Non-emergency line:</strong> 403-266-1234</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Mailing address:</strong></p>
<!-- /wp:paragraph -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><p>Calgary Police Service<br>5111 47 St. N.E.<br>Calgary, Alberta, Canada<br>T3J 3R2 </p></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph -->
<p><strong>General email:</strong> <a href="mailto:cps@calgarypolice.ca">cps@calgarypolice.ca</a><em>(Please note that this address is not for emergency use and is  not monitored on a 24-hour basis. The Calgary Police Service will make  every effort to respond to your comments as quickly as possible, but due  to the volume of email messages the CPS receive, some delays may be  unavoidable.)</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Social media:</strong> Connect with us on <a href="https://mobile.twitter.com/calgarypolice" target="_blank" rel="noreferrer noopener">Twitter</a>, <a href="https://www.facebook.com/CalgaryPolice" target="_blank" rel="noreferrer noopener">Facebook</a> and <a href="http://www.youtube.com/user/calgarypolice" target="_blank" rel="noreferrer noopener">Youtube</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Telephone directory:</strong> Please see our complete <a href="http://www.calgary.ca/cps/Pages/Calgary-Police-phone-directory.aspx">phone directory</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>District offices:</strong> Please view our <a href="http://www.calgary.ca/cps/Pages/Calgary-Police-Service-district-offices.aspx">district offices listing</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Recruiting:</strong> Please view our <a href="http://join.calgarypolice.ca/">Recruiting website.</a></p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Additional services</h2>
<!-- /wp:heading -->

<!-- wp:list -->
<ul><li><strong>To report an anonymous crime tip</strong>, please contact Calgary Crime Stoppers​.</li><li><strong>Victims of crime or tragedy</strong> need to contact our <a href="http://www.calgary.ca/cps/Pages/Community-programs-and-resources/Victims-of-crime/Victim-Assistance-Support-Team.aspx">Victim Assistant Team</a> for information and support.</li><li><strong>People looking​ for security clearances</strong> need to contact the <a href="http://www.calgary.ca/cps/Pages/Public-services/Police-information-checks.aspx">Police Information Check Unit</a>.</li><li><strong>Bylaw infractions</strong> can be <a href="https://calgary-csrprodcwi.motorolasolutions.com/Public/ServiceRequest.mvc/SRIntakeStep3?guid=d9bf00b822db4f46a4b80aec4e68031d" target="_blank" rel="noreferrer noopener">reported online</a> to The City of Calgary or by calling 3-1-1.</li></ul>
<!-- /wp:list -->";s:10:"post_title";s:22:"Calgary Police Service";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"inherit";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:15:"266-revision-v1";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-02-13 16:53:21";s:17:"post_modified_gmt";s:19:"2019-02-13 23:53:21";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:266;s:4:"guid";s:33:"https://cps-lits/266-revision-v1/";s:10:"menu_order";i:0;s:9:"post_type";s:8:"revision";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}