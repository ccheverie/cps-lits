��d\<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:224;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-02-13 14:45:12";s:13:"post_date_gmt";s:19:"2019-02-13 21:45:12";s:12:"post_content";s:3629:"<!-- wp:paragraph -->
<p>An EPO is intended for victims and family
members affected by family violence who require immediate protection as per the
Protection Against Family Violence Act.&nbsp; Victims
in the Calgary and Edmonton area may attend the Court House 24 hours a day to
apply for an Emergency Protection Order.&nbsp;
In all other areas a victim can seek assistance in making application
from their local police or victims services.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>An EPO can order an abuser to not be near or make contact with you and any named family members.  The abuser can be ordered removed from the home; even if the abuser resides there or owns the property.  This will provide you the time needed to make additional safety arrangements for yourself and children.  There is no cost to obtain an EPO.  All claimants and respondents party to an EPO are provided with free legal support for the EPO Review through Legal Aid Alberta regardless of income. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="http://www.humanservices.alberta.ca/documents/protection-against-family-violence-act-guide.pdf " target="_blank" rel="noreferrer noopener" aria-label="Read more at Human Services Alberta. (opens in a new tab)">Read more at Human Services Alberta.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>After
an EPO is granted</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The respondent must be served the EPO.&nbsp; DO NOT serve the EPO yourself as this is not
safe. </p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>If you live in the Calgary or
Edmonton area, your EPO will automatically be forwarded to the police and they
will serve it for you. </li><li>If you live in any other
municipality, deliver a copy of the EPO to the police station or RCMP
detachment closest to where you reside. </li><li>Police will contact you when
the EPO has been served.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>Once the EPO has been served, you should
also give it to anyone that needs to know, such as:</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Your workplace security or
employer (If you choose to advise them)</li><li>Children’s schools, child care
centres, etc.</li><li>Any other persons named on the
EPO for protection (i.e. other family members).</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Attending
the EPO Review</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The EPO has a date on it when the court
will review your case and decide whether to revoke it, continue it or replace
it with a different order.&nbsp; You need to
attend this review.</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Date and time of your review
(found on line #7 of your EPO)</li><li>Call the EPO Program at least
24 hours prior to the review date:</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>Calgary &amp; Area: 403-297-5260</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Edmonton &amp; Area: 780-422-9222</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Other Locations: 1-866-845-3425</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Arrange for a friend or
relative to attend with you as the respondent may be present for the review. </li><li>Arrive at the specified court
room for review at least 30 minutes early.</li><li>Do not leave the review until
your case has been heard.</li><li>If at any time you feel unsafe in
the court house or the respondent has made contact with you seek a Sheriff or
security officer within the court house immediately. </li></ul>
<!-- /wp:list -->";s:10:"post_title";s:32:"Emergency Protection Order (EPO)";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"inherit";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:15:"222-revision-v1";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-02-13 14:45:12";s:17:"post_modified_gmt";s:19:"2019-02-13 21:45:12";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:222;s:4:"guid";s:33:"https://cps-lits/222-revision-v1/";s:10:"menu_order";i:0;s:9:"post_type";s:8:"revision";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}