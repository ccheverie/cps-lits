�-v\<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"367";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-02-22 15:14:37";s:13:"post_date_gmt";s:19:"2019-02-22 22:14:37";s:12:"post_content";s:3791:"<h1>Male Domestic Abuse Outreach Program</h1>
Men experience abuse too. Half the problem is asking for help. Half the problem is being believed.

The Calgary Counselling Centre Male Domestic Abuse Outreach Program provides counselling, advocacy, social service referrals (housing, financial aid, legal guidance, support) to men and their families experiencing domestic abuse of all forms.

Our program is adaptable. It’s flexible. And focused on each individual client. Whether it’s an informal conversation at a coffee shop or in a more traditional setting at <a href="https://connectline.ca/calgary-counseling-centre/">Calgary Counselling Centre</a>.

The program is strongly focused on community collaboration. We work with other service providers in the community to better serve men and their families.

We focus on helping men access the services and gain the support they need to change their lives for the better.

The Male Domestic Abuse Outreach Program is funded by Alberta Children’s Services. It serves Calgary, Strathmore, and surrounding rural areas. There is access to the program at Calgary Counselling Centre, the Community Crisis Society’s Wheatland Shelter, and in the community.

&nbsp;
<h4>Forms of Abuse</h4>
Abuse can come in many different forms and all can be equally difficult to live with, including:
<ul>
 	<li>Emotional: put-downs, isolation, restricting freedoms</li>
 	<li>Intimidation: words or actions that may be used to scare someone. May include destroying property, threatening, stalking, or harassing behaviour</li>
 	<li>Physical: activities that can cause physical pain or injury</li>
 	<li>Sexual: use of force or pressure on one to have sex in a way they do not wish to, criticizing performance, withholding sex or affection as punishment</li>
 	<li>Financial: Making or attempting to make a person financially dependent, e.g., maintaining total control over financial resources and withholding access to money</li>
 	<li>Neglect: abuse directed at a person who is dependent on personal care: children, persons with disabilities/illness, or seniors</li>
 	<li>Religious: restricting or preventing one from engaging in spiritual practices, customs, or traditions, or using one’s personal beliefs to exploit them</li>
</ul>
If you have experienced any of the above, then you also may feel:
<ul>
 	<li>Fear</li>
 	<li>Shame</li>
 	<li>Silenced</li>
 	<li>Confused</li>
 	<li>Hurt</li>
 	<li>Disappointed</li>
 	<li>Worthless</li>
 	<li>Helplessness or hopelessness</li>
 	<li>Emasculated</li>
 	<li>Guilty</li>
 	<li>Anxious and/or depressed</li>
</ul>
<h4 id="help">How we can help</h4>
If you feel that you may have experienced any of the above types of abuse toward you, or you need someone to talk to, then we are here to help.

We are able to offer counselling or outreach support services to assist you through this difficult time.

Please contact our <a href="mailto:alex.cameron@calgarycounselling.com">Men’s Domestic Abuse Outreach Coordinator</a> by email or phone number below and he will be happy to find ways to assist you.

If you are experiencing any of the above and feel unsafe in your home, you can also contact the <a href="http://strathmoreshelter.com/" target="_blank" rel="noopener noreferrer">Wheatland Crisis Shelter</a> located in Strathmore Alberta. The shelter works with men who need a safe place when fleeing from domestic abuse.
<h4 id="contact">Contact our Men’s Outreach Coordinator or register for counselling below:</h4>
Marcus Cheung, MSW, Registered Social Worker
Male Domestic Abuse Outreach Program Coordinator
Phone: 403-691-5964
Cell: 403.651.8075
E: <a href="mailto:marcus.cheung@calgarycounselling.com">marcus.cheung@calgarycounselling.com</a>";s:10:"post_title";s:24:"Men Experience Abuse Too";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:24:"men-experience-abuse-too";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-02-22 15:23:11";s:17:"post_modified_gmt";s:19:"2019-02-22 22:23:11";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:29:"https://connectline.ca/?p=367";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}