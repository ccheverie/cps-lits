 �i]<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"225";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-02-13 14:47:16";s:13:"post_date_gmt";s:19:"2019-02-13 21:47:16";s:12:"post_content";s:1614:"<!-- wp:paragraph -->
<p>A QBPO is another protection order available
from the Protection Against Family Violence Act intended for victims and
families affected by family violence.&nbsp;
This order is not intended for emergent situations.&nbsp; An EPO upon review may be replaced by a QBPO
or a victim may apply for this order during regular court hours at the Court of
Queen’s Bench.&nbsp; A QBPO can order the same
provisions as an EPO with additional provisions for reimbursement of monetary
losses, giving temporary possession of personal property and order counselling
for your partner or other family members.&nbsp;
Application forms are available at the Court of Queen’s Bench.&nbsp; You will be advised of the process and your
responsibilities at the court house.&nbsp; </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Not all court houses in the Province of
Alberta have a Court of Queen’s Bench, see link for a location nearest to
you.&nbsp; <a href="https://albertacourts.ca/qb/about/locations-and-sittings">https://albertacourts.ca/qb/about/locations-and-sittings</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>For more information on the Queen’s Bench Protection Order, read  <a href="http://www.humanservices.alberta.ca/documents/protection-against-family-violence-act-guide.pdf" target="_blank" rel="noreferrer noopener" aria-label="http://www.humanservices.alberta.ca/documents/protection-against-family-violence-act-guide.pdf (opens in a new tab)">http://www.humanservices.alberta.ca/documents/protection-against-family-violence-act-guide.pdf</a>.</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:39:"Queen’s Bench Protection Order (QBPO)";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:34:"queens-bench-protection-order-qbpo";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-02-13 14:47:18";s:17:"post_modified_gmt";s:19:"2019-02-13 21:47:18";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:23:"https://cps-lits/?p=225";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}