 �i]<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"271";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-02-13 21:13:41";s:13:"post_date_gmt";s:19:"2019-02-14 04:13:41";s:12:"post_content";s:3483:"<!-- wp:paragraph -->
<p><em>“Peer support is ‘the process by which like-minded individuals with similar experiences – who have traveled or are travelling the road – encourage and assist each other to continue the recovery journey.’” – Canadian Coalition of Alternative Mental Health Resources</em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Our Peer Support service provides the opportunity for anyone – individual, family member or professional – looking for a connection with others, who is new to the mental health and/or addiction community, or looking for&nbsp;information about a mental health diagnosis, the chance to speak with someone with personal experience. Our peers provide:</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>a warm welcome</li><li>understanding</li><li>knowledge of the mental health and/or addiction community</li><li>connection to mental health and/or addiction services</li><li>a bridge to services in the community</li><li>emotional and social support to others who share a common experience.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>When you call CMHA the first person you talk to will be someone who may have travelled a similar journey to your own who can assist&nbsp;and provide you with information on your options, opportunities and supports available. They have had over 120 hours of training on relationships, recovery and peer support and a lifetime of experience. Finally, our peer workers are special because they are able to connect with individuals that are often left feeling disconnected by the health system as they share a common experience and understanding with those they speak with.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>CMHA believes strongly in peer support because research shows us that when you make a connection with someone with lived experience of mental health and/or addiction, you are more likely to: engage with community, see a decrease in symptoms, move through the recovery journey, develop skills, and access crisis supports less often.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Peer support program services can be accessed over the phone at 403-297-1402, face-to-face at the CMHA Welcome Centre (<a href="https://www.google.ca/maps?q=%23105,+1040+-+7+Avenue+SW&amp;rlz=1C1CHBF_enCA723CA723&amp;um=1&amp;ie=UTF-8&amp;sa=X&amp;ved=0ahUKEwjo0o6S5vPYAhUL2mMKHVBTCyoQ_AUICigB" target="_blank" rel="noreferrer noopener">#105, 1040 – 7 Avenue SW</a>) or through email at&nbsp;<a href="mailto:peer@cmha.calgary.ab.ca">peer@cmha.calgary.ab.ca</a>. There is always a peer available to connect individually from 9:00 am – 4:00 pm Monday to Friday, and Tuesday and Wednesday from 4:00 pm – 7:00 pm (excluding holidays).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>Please note: CMHA – Calgary does not provide crisis services. If you or someone you know is in crisis, please call the&nbsp;</em><a href="http://www.distresscentre.com/" target="_blank" rel="noreferrer noopener"><strong><em>Distress Centre’</em></strong></a><em><a href="http://www.distresscentre.com/" target="_blank" rel="noreferrer noopener"><strong>s</strong></a>&nbsp;24-hr Crisis Line at (403) 266-HELP or visit their&nbsp;</em><a href="http://www.distresscentre.com/" target="_blank" rel="noreferrer noopener"><strong><em>website.</em></strong></a><em>&nbsp;If you or a person you know is at risk of harm, please call 9-1-1.</em></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:27:"CMHA Calgary - Peer Support";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:25:"cmha-calgary-peer-support";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-02-13 21:13:43";s:17:"post_modified_gmt";s:19:"2019-02-14 04:13:43";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:23:"https://cps-lits/?p=271";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}