<?php
/*
* Template Name: LITS RESULTS Page
*/

get_header(); ?>

    <div class="">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
            <div class="container">
                <div class="card mt-4">
                  <div class="card-body">

                <?php
                $lits_option_value = get_transient( 'lits_option_value' );
                foreach(get_transient('lits_type_value') as $litsType) {
                    $lits_type_value = $lits_type_value . ',' . $litsType;
                }
                set_transient( 'lits_resource_value', $_POST["lits-resource"], 12 * HOUR_IN_SECONDS );
                foreach(get_transient('lits_resource_value') as $litsResource) {
                    $lits_resource_value = $lits_resource_value . ',' . $litsResource;
                }
         
                $category_values = $lits_option_value . $lits_type_value . $lits_resource_value;
                //echo $category_values;

                $the_query = new WP_Query( array( 'category_name' => $category_values ) );

                // The Loop
                if ( $the_query->have_posts() ) {
                    echo '<ul>';
                    while ( $the_query->have_posts() ) {
                        $the_query->the_post();
                        echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
                    }
                    echo '</ul>';
                    /* Restore original Post Data */
                    wp_reset_postdata();
                } else {
                    // no posts found
                }
                ?>
                  </div>
                </div>
              </div>
            </main><!-- #main -->
        </div><!-- #primary -->
    </div>

<?php
get_footer();
