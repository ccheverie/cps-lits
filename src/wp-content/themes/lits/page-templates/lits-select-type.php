<?php
/*
* Template Name: LITS Select TYPE Page
*/

get_header(); ?>

    <div class="">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
            <div class="container">
                <div class="card mt-4">
                  <div class="card-body">

                <?php
                while ( have_posts() ) : the_post();
                // Capture info from Select Option screen
                set_transient( 'lits_option_value', $_POST["lits-option"], 12 * HOUR_IN_SECONDS );
                echo "Option selected:<br>";
                echo '<p style="color:red">' . get_transient( 'lits_option_value' ) . '</p>';

                  the_content();

                  //echo 'See results <a href="/?tag=verbal-abuse,sexual-abuse">here</a>';
?>
            <form method="POST" action="/select-resource">
                <fieldset>         
                    <label><input type="checkbox" name="lits-type[]" value="elder-abuse">Elder Abuse</label><br>                      
                    <label><input type="checkbox" name="lits-type[]" value="emotional-abuse">Emotional Abuse</label><br>      
                    <label><input type="checkbox" name="lits-type[]" value="financial-abuse">Financial Abuse</label><br>      
                    <label><input type="checkbox" name="lits-type[]" value="physical-abuse">Physical Abuse</label><br>
                    <label><input type="checkbox" name="lits-type[]" value="sexual-abuse">Sexual Abuse</label><br>      
                    <label><input type="checkbox" name="lits-type[]" value="spiritual-abuse">Spiritual Abuse</label><br>   
                    <label><input type="checkbox" name="lits-type[]" value="verbal-abuse">Verbal Abuse</label><br>    
                    <label><input type="checkbox" name="lits-type[]" value="sexual-exploitation">Sexual Exploitation</label><br>      
                    <label><input type="checkbox" name="lits-type[]" value="stalking-harassment">Stalking / Harassment</label><br>      
                    <br> 
                    <a class="btn btn-outline-secondary" href="/select-option" role="button">Previous</a> <button class="btn btn-primary" type="submit">Next</button>       
                    <br>
                </fieldset>     
            </form>
<?php


                    // If comments are open or we have at least one comment, load up the comment template.
                    //if ( comments_open() || get_comments_number() ) :
                    //    comments_template();
                    //endif;

                endwhile; // End of the loop.
                ?>
                  </div>
                </div>
              </div>
            </main><!-- #main -->
        </div><!-- #primary -->
    </div>

<?php
get_footer();
