<?php
/*
* Template Name: LITS Template
*/

get_header(); ?>

    <div class="">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
            <div class="container">
                <div class="card mt-4">
                  <div class="card-body">
                <?php
                while ( have_posts() ) : the_post();


                  the_content();


                    // If comments are open or we have at least one comment, load up the comment template.
                    // if ( comments_open() || get_comments_number() ) :
                    //     comments_template();
                    // endif;

                endwhile; // End of the loop.
                ?>
                  </div>
                </div>
              </div>
            </main><!-- #main -->
        </div><!-- #primary -->
    </div>

<?php
get_footer();
