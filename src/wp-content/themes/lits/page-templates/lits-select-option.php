<?php
/*
* Template Name: LITS Select OPTION Page
*/

get_header(); ?>

    <div class="">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
            <div class="container">
                <div class="card mt-4">
                  <div class="card-body">

                <?php
                while ( have_posts() ) : the_post();
                  
                the_content();
            ?>

            <form method="POST" action="/select-type">
                <fieldset>  
                    <div>
                        <input type="radio" id="option-male" name="lits-option" value="male">
                        <label for="option-male">Male</label>
                    </div>
                    <div>
                        <input type="radio" id="option-female" name="lits-option" value="female">
                        <label for="option-female">Female</label>
                    </div>
                    <div>
                        <input type="radio" id="option-professional" name="lits-option" value="professional">
                        <label for="option-professional">Professional</label>
                    </div>
                    <div>
                        <input type="radio" id="option-non-binary" name="lits-option" value="non-binary">
                        <label for="option-non-binary">Non Binary / Non Gender Conforming</label>
                    </div>
                    <div>
                        <input type="radio" id="option-friends-family" name="lits-option" value="friends-family" required>
                        <label for="option-friends-family">Family / Friends</label>
                    </div>     
                    <br><br>       
                    <button class="btn btn-primary" type="submit">Next</button>
                </fieldset>  
            </form>

            <?php
                    // If comments are open or we have at least one comment, load up the comment template.
                    //if ( comments_open() || get_comments_number() ) :
                    //    comments_template();
                    //endif;

                endwhile; // End of the loop.
                ?>
                  </div>
                </div>
              </div>
            </main><!-- #main -->
        </div><!-- #primary -->
    </div>

<?php
get_footer();
