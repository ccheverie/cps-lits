<?php
/*
* Template Name: LITS Home Page
*/

get_header(); ?>

    <div class="">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">


                <div class="search-container">
                    <form role="search" method="get" id="searchform" action="/category/services">
                    <input type="text" value="" name="s" id="s" placeholder="Find services" />
                    <!--input type="submit" id="searchsubmit" value="Search" /-->
                    </form>
                </div> <!--/.search-container -->
                        
                <section id="section-services" class="home-scroller-section">
                    
                    <h2 class="heading">Helping Services</h2>

                    <div class="card-scroller">

                        <a href="/category/domestic-violence" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-1.jpg');">
                            <div class="card-body text-white">
                                <h5 class="card-title">Domestic Violence</h5>
                                <!-- <p class="card-text">Content goes here.</p> -->
                            </div>
                        </a> <!--/.card-->
                        
                        <a href="/category/safety-shelter" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-2.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Safety/Shelter</h5>                               
                            </div>
                        </a> <!--/.card-->

                        <a href="/category/counselling-services" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-4.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Counselling</h5>
                            </div>
                        </a> <!--/.card-->      
                        
                        <a href="/category/addiction" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-5.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Addiction Treatment</h5>
                            </div>
                        </a> <!--/.card-->     
  
                        <a href="/category/mental-health" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-6.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Mental Health</h5>
                            </div>
                        </a> <!--/.card-->   

                        <a href="/category/lgbtq" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-10.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">LGBTQ+</h5>
                            </div>
                        </a> <!--/.card-->     
                        
                        <a href="/category/newcomers" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-8.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Newcomers</h5>
                            </div>
                        </a> <!--/.card-->  

                        <a href="/category/sexual-abuse" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-3.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Sexual Abuse</h5>
                            </div>
                        </a> <!--/.card-->                        
   
                        <a href="/category/youth" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-1.jpg');">
                            <div class="card-body text-white">
                                <h5 class="card-title">Youth</h5>
                                <!-- <p class="card-text">Content goes here.</p> -->
                            </div>
                        </a> <!--/.card-->

                        <a href="/category/elder-abuse" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-9.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Elder Abuse</h5>
                            </div>
                        </a> <!--/.card-->   

                        <a href="/category/stalking-harassment" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-7.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Stalking/Harassment</h5>
                            </div>
                        </a> <!--/.card-->  

                        <a href="/category/sex-work" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-2.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Sex Work</h5>                               
                            </div>
                        </a> <!--/.card-->

                        <a href="/category/housing" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-4.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Housing</h5>
                            </div>
                        </a> <!--/.card-->      
                        
                        <a href="/category/men" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-5.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Men</h5>
                            </div>
                        </a> <!--/.card-->     
  
                        <a href="/category/legal" class="card overlay-grad" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banners/banner-6.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Legal</h5>
                            </div>
                        </a> <!--/.card-->   

                    </div> 
                </section> <!--/#section-services-->
                

                <section id="section-questions" class="home-scroller-section">
                    
                    <h2 class="heading">Questions &amp; Education</h2>

                    <div class="card-scroller alt">

                        <a href="/category/questions/types-of-violence" class="card card-gradient" style="background-image: url('/wp-content/uploads/2019/02/warning.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Types of Violence</h5>
                            </div>
                        </a> <!--/.card-->   

                        <a href="/category/questions/safety-planning" class="card card-gradient" style="background-image: url('/wp-content/uploads/2019/06/safety-small.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Safety Planning</h5>
                            </div>
                        </a> <!--/.card-->   

                        <a href="/category/questions/cyber-safety" class="card card-gradient" style="background-image: url('/wp-content/uploads/2019/02/cyber-safety.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Cyber Safety</h5>
                            </div>
                        </a> <!--/.card-->   

                        <a href="/category/questions/provincial-acts" class="card card-gradient" style="background-image: url('/wp-content/uploads/2019/01/banner-legislature-alberta.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Provincial Acts</h5>
                            </div>
                        </a> <!--/.card-->   
                    
                        <a href="/category/questions/court-orders" class="card card-gradient" style="background-image: url('/wp-content/uploads/2019/01/banner-gavel.jpg');">
                            <div class="card-body text-white">
                            <h5 class="card-title">Court Orders</h5>
                            </div>
                        </a> <!--/.card-->   

                    <?php 
                    // Sorted alphabetically
                    /*
                    query_posts($query_string . '&cat=38&tag=featured&orderby=title&order=DESC');
                    if ( have_posts() ) :
                        while ( have_posts() ) : the_post();


                        <a href="<?php echo esc_url( get_permalink() ); ?>" class="card card-gradient" style="background-image: url('<?php the_post_thumbnail_url('large'); ?>');">
                            <div class="card-body text-white">
                                <h5 class="card-title">
                                <?php $categories = get_the_category();
                                if ( ! empty( $categories ) ) {
                                } 
                                $postcat = get_the_category( $post->ID );
                                if ( ! empty( $postcat ) ) {
                                    if ( $postcat[0]->name == "Questions") {
                                        echo esc_html( $postcat[1]->name );  
                                    } else {
                                        echo esc_html( $postcat[0]->name );  
                                    } 
                                }
                                ?>                        
                                </h5>
                                <p class="card-text"><?php the_title() ?></p>
                            </div>
                        </a>                         
                    */    
                    ?>



                    <?php
                    /*
                    endwhile;
                    endif; 
                    */
                    ?>

                    </div> 
                </section> <!--/#section-questions-->
                
                
            </main><!-- #main -->
        </div><!-- #primary -->
    </div>

<?php
get_footer();