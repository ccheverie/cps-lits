<?php
/*
* Template Name: LITS Start Page
*/

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">

            <div id="loading-content">
                <h1>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-large.png" alt="Connect Line" />
                    <span>Draw Your Line in the Sand</span>
                </h1>

                <div class="loading-content__bottom">
                    <p>Calgary's local connection to helpful services & information</p>
                    <a class="btn btn-primary btn-green"  href="<?php echo esc_url( home_url( '/' ) ); ?>">Continue to app</a><br> 
                    <a class="btn btn-primary" href="tel:4032347233">Call Connect Line</a> &nbsp;&nbsp;&nbsp; <a class="btn btn-primary" href="tel:911">Get Help - Call 911</a>                   
                </div>
            </div> <!--/ #loading-content -->

            <div id="loading-spinner">
                <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                <p>Loading . . .</p>
            </div> <!--/ #loading-spinner -->

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();