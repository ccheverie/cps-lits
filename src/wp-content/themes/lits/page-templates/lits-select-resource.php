<?php
/*
* Template Name: LITS Select RESOURCE Page
*/

get_header(); ?>

    <div class="">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
            <div class="container">
                <div class="card mt-4">
                  <div class="card-body">

                <?php
                while ( have_posts() ) : the_post();
                echo "Option selected:<br>";
                echo '<p style="color:red">' . get_transient( 'lits_option_value' ) . '</p>';
                // Capture info from Select Type screen
                set_transient( 'lits_type_value', $_POST["lits-type"], 12 * HOUR_IN_SECONDS );
                echo 'Types selected:<br><p style="color:red">';
                foreach(get_transient('lits_type_value') as $litsType) {
                    echo '' . $litsType. '<br>';
                }
                echo '</p>';

                the_content();

?>
            <form method="POST" action="/results">
                <fieldset>         
                    <label><input type="checkbox" name="lits-resource[]" value="counselling">Counselling</label><br>    
                    <label><input type="checkbox" name="lits-resource[]" value="court-orders">Court Orders</label><br>      
                    <label><input type="checkbox" name="lits-resource[]" value="financial-support">Financial Support</label><br>      
                    <label><input type="checkbox" name="lits-resource[]" value="provincial-acts">Provincial Acts</label><br>      
                    <label><input type="checkbox" name="lits-resource[]" value="resources">Resources</label><br>      
                    <label><input type="checkbox" name="lits-resource[]" value="safety-planning">Safety Planning</label><br> 
                    <label><input type="checkbox" name="lits-resource[]" value="shelters">Shelters</label><br>                                                             
                    <label><input type="checkbox" name="lits-resource[]" value="warning-signs">Warning Signs</label><br>
                    <br>      
                    <button class="btn btn-primary" type="submit">View Results</button>    
                    <br>
                </fieldset>     
            </form>
<?php


                    // If comments are open or we have at least one comment, load up the comment template.
                    //if ( comments_open() || get_comments_number() ) :
                    //    comments_template();
                    //endif;

                endwhile; // End of the loop.
                ?>
                  </div>
                </div>
              </div>
            </main><!-- #main -->
        </div><!-- #primary -->
    </div>

<?php
get_footer();
