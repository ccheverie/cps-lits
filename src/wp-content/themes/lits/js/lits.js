
$ = jQuery;

// Boss Mode: exit quickly from site
function exitSite() {
    var $body = $(document.body);

    $('footer #footer-navigation ul#menu-navigation-menu li.menu-exit a').on('touch click', function(e){    
        $body.css({
            display: 'none'
        });

        $(document).attr("title", "Google");
        //location.replace(this.href);
        window.open("https://www.theweathernetwork.com/ca/weather/alberta/calgary", "_newtab");
        // Replace current site with another benign site
        window.location.replace('http://google.com');        
        e.preventDefault();
    });
}

function saveToProfile() {
    $('.simplefavorite-button').on('touch click', function(){
        $('.menu-profile a').addClass('highlight');
        setTimeout(function(){
            $('.menu-profile a').removeClass('highlight');
       }, 2000);
    });
}

function loadingStartPage() {

    setTimeout(function(){ 

        $("#loading-spinner").fadeOut("fast", function() {
            // Animation complete. 
            // TODO: preload some images here
            $("#loading-content").fadeIn();
          });        
     }, 4000);
}

$(document).ready(function() {

    saveToProfile();
    exitSite();
    loadingStartPage();

    // Add to home screen functionality
    addToHomescreen();
    function showPosition(position) {
        console.log('Latitude: '+position.coords.latitude+'Longitude: '+position.coords.longitude);
    }

    function showError(error) {
        console.log(error.code);

        // switch(error.code) {
        //     case error.PERMISSION_DENIED:
        //         alert("User denied the request for Geolocation.");
        //         break;
        //     case error.POSITION_UNAVAILABLE:
        //         alert("Location information is unavailable.");
        //         break;
        //     case error.TIMEOUT:
        //         alert("The request to get user location timed out.");
        //         break;
        //     case error.UNKNOWN_ERROR:
        //         alert("An unknown error occurred.");
        //         break;
        // }
    }


    if (navigator.geolocation) {
        // Get the user's current position
        var optn = {
			enableHighAccuracy : true,
			timeout : Infinity,
			maximumAge : 0
        };     
        console.log('Geolocation IS supported in your browser');
        //navigator.geolocation.getCurrentPosition(showPosition, showError, optn);
    } else {
        console.log('Geolocation is NOT supported in your browser');
    }

    // $.getJSON("https://api.ipstack.com/68.146.44.112?access_key=6b250454be6cec9284ca30b2f872620b&output=json&use_https=false", function(data) {
    //     console.log(data);
    //     var country_code = data.country_code;
    //     var country = data.country_name;
    //     var ip = data.ip;
    //     var time_zone = data.time_zone;
    //     var latitude = data.latitude;
    //     var longitude = data.longitude;
    
    //     console.log("Country Code: " + country_code);
    //     console.log("Country Name: " + country);
    //     console.log("IP: " + ip); 
    //     console.log("Time Zone: " + time_zone);
    //     console.log("Latitude: " + latitude);
    //     console.log("Longitude: " + longitude);   
    // });
    
    // $.get("https://freegeoip.net/json/", function (response) {
    //     $("#ip").html("IP: " + response.ip);
    //     $("#address").html("Location: " + response.city + ", " + response.region_name);
    //     $("#details").html(JSON.stringify(response, null, 4));
    // }, "jsonp");

});
