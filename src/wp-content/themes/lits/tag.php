<?php
/**
 * The template for displaying Questions category (landing) pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WP_Bootstrap_4
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <div class="container">
            <div class="row">
                <div class="col-12 wp-bp-content-width">
                    
            <?php if ( in_category('questions') ) { ?>
                    
                <h1 class="heading-1">Questions & Education</h1>
                    
                <div id="services-filter" class="form-row align-items-center">
                    <div class="col-auto">
                        <label for="tag">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-filter.png" alt=""/>Filter -
                        </label>
                    </div>
                    <div class="col">

                    <?php 
// $post_tags = array();
// $the_query = new WP_Query( 'cat=38' );
// if ( $the_query->have_posts() ) : 
// while ( $the_query->have_posts() ) : $the_query->the_post(); 
// $posttags = get_the_tags();
// foreach( $posttags as $posttag )  {
// $post_tags[$posttag->slug] = $posttag->name;
// }
// endwhile;
// endif;

//wp_reset_query();




$args = array(
    'name'               => 'my_tags',
    'order'              => 'ASC',
    'orderby'            => 'name',
    'hide_empty'         => 0,
    'taxonomy'           => 'post_tag',
    'value_field'        => 'slug',
    'selected'        => true,
    'hierarchical' => 0
);
wp_dropdown_categories($args); 
?>
<script type="text/javascript">
var dropdown = document.getElementById("my_tags");
function onCatChange() {
//if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
location.href = "<?php echo esc_url( home_url( '/' ) ); ?>/questions/"+dropdown.options[dropdown.selectedIndex].value;
//}
}
dropdown.onchange = onCatChange;
</script>




                <?php
                    // Sorted alphabetically
                    query_posts($query_string . '&orderby=title&order=ASC');
                    if ( have_posts() ) :

                    /* Start the Loop */
                    while ( have_posts() ) : the_post();
                    ?>

                        <div id="location-<?php the_ID(); ?>" class="row no-gutters location">
                            <div class="col-8 col-md-9">
                                <a class="location__details" href="<?php echo esc_url( get_permalink() ); ?>">
                                    <h2 class="location__name"><?php the_title() ?></h2>
                                    <p class="location__subtitle"><?php if( get_field('sub_title') ): the_field('sub_title'); endif; ?></p>
                                    <div class="location__address">
                                        <?php if( get_field('street_address') ): trim(the_field('street_address')); endif; ?><?php if( get_field('street_address') && get_field('city') ): ?>, <?php endif; ?>  
                                        <?php if( get_field('city') ): the_field('city'); endif; ?>                                        
                                        <!--<span class="location__distance">(2.4 km away)</span>-->
                                    </div>

                                <?php if( get_field('hours') ): ?>                               
                                    <div class="location__hours">
                                        <strong>Open:</strong> <?php the_field('hours'); ?>
                                    </div>
                                <?php endif; ?> 
                                </a>
                            </div>
                            <div class="col-4 col-md-3">

                            <?php if( get_field('phone') ): ?>  
                                <a href="tel:<?php the_field('phone'); ?>" class="btn btn-primary btn-sm">Call Now</a>
                            <?php endif; ?> 
                                
                            <?php 
                            $location = get_field('location');
                            if( !empty($location) ):
                            ?>
                                <a href="https://www.google.com/maps/search/?api=1&query=<?php echo $location['lat']; ?>,<?php echo $location['lng']; ?>" target="_blank" rel="noreferrer" class="btn btn-primary btn-sm">Directions</a>
                            <?php endif; ?> 

                            </div>
                        </div> <!--/.location-<?php the_ID(); ?>--> 

                <?php
                        //get_template_part( 'template-parts/services', get_post_format() );
					endwhile;

						the_posts_navigation( array(
							'next_text' => esc_html__( 'Newer Posts', 'wp-bootstrap-4' ),
							'prev_text' => esc_html__( 'Older Posts', 'wp-bootstrap-4' ),
						) );

                else : 
                    //get_template_part( 'template-parts/content', 'none' );
                    echo '<p>Sorry, there are currently no results for this Service.</p>';
                endif; 
                    
            } // end 'else' on Questions category ?>
                                        
                </div> <!-- /.col-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->

    </main> <!--#main-->
</div> <!--/#primary-->   

<?php
get_footer();
