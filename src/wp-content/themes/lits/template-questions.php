<?php
/*
* Template Name: Questions Template
*/

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <div class="container">
            <div class="row">
                <div class="col-12 wp-bp-content-width">
                    
                <h1 class="heading-1">Questions & Education</h1>
                  
                <div id="services-filter" class="form-row align-items-center">
                    <div class="col-auto">
                        <label for="tag">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-filter.png" alt=""/>Filter -
                        </label>
                    </div>
                    <div class="col">
    <select name="tag" id="tag" class="postform">
    <option value="all" selected="selected">All Questions</option>
    <?php  foreach( $post_tags as $tagslug => $tagname ){ ?>
        <!--li><a href="#filter-tags-<?php echo $tagslug;?>" data-filter-value="<?php echo     $tagslug;?>"><?php echo $tagname;?></a></li-->
        <option value="<?php echo $tagslug;?>""><?php echo $tagname;?></option>
    <?php } ?>
	<!-- <option class="level-0" value="39">All Services</option>
	<option class="level-0" value="25">Elder Abuse</option>
	<option class="level-0" value="11">Emotional Abuse</option>
	<option class="level-0" value="12">Financial Abuse</option>
	<option class="level-0" value="9">Physical Abuse</option>
	<option class="level-0" value="10">Sexual Abuse</option>
	<option class="level-0" value="24">Sexual Exploitation</option>
	<option class="level-0" value="26">Spiritual Abuse</option>
	<option class="level-0" value="13">Stalking/Harassment</option>
	<option class="level-0" value="8" selected="selected">Verbal Abuse</option> -->
    </select>


                        <script type="text/javascript">
                        var dropdown2 = document.getElementById("tag");
                        function onTagChange() {
                            console.log('change');
                            if ( dropdown2.options[dropdown2.selectedIndex].value > 0 ) {
                            location.href = "<?php echo esc_url( home_url( '/' ) ); ?>/questions/"+dropdown2.options[dropdown2.selectedIndex].value;
                            }
                        }
                        dropdown2.onchange = onTagChange;
                        </script>
                    </div> <!--/.col-->
                </div>   <!--/#services-filter--> 

                     
                <?php 
$post_tags = array();
$the_query = new WP_Query( 'posts_per_page=5&cat=38' );
if ( $the_query->have_posts() ) : 
while ( $the_query->have_posts() ) : $the_query->the_post(); 
$posttags = get_the_tags();
foreach( $posttags as $posttag )  {
$post_tags[$posttag->slug] = $posttag->name;
}
endwhile;
endif;
wp_reset_query();

            
?>
                             
                             
                </div> <!-- /.col-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->

    </main> <!--#main-->
</div> <!--/#primary-->   

<?php
get_footer();


