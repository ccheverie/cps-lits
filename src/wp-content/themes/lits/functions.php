<?php
add_action( 'wp_enqueue_scripts', 'enqueue_parent_theme_style' );
function enqueue_parent_theme_style() {
    wp_enqueue_style( 'load-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('font-lato','https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i');
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}


//add_action( 'wp_enqueue_scripts', 'enqueue_load_fa', 98 );
// Add CSS & JavaScript
function lits_scripts_css() {
    
    wp_enqueue_style('addtohomescreen-css', get_stylesheet_directory_uri().'/vendor/addtohomescreen.css');
    //wp_enqueue_style('lits-css', get_stylesheet_directory_uri().'/style.css');
    wp_enqueue_script('addtohomescreen-js', get_stylesheet_directory_uri().'/vendor/addtohomescreen.min.js','',' 1.1', true);
    wp_enqueue_script('lits-js', get_stylesheet_directory_uri().'/js/lits.js','',' 1.1', true);
}
add_action('wp_enqueue_scripts', 'lits_scripts_css', 99);

// API Key for ACF google map picker
function my_acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyDQyWZUBA9Npv2vOi1pnQo0jxZp5j3iSh4';
	return $api;
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
