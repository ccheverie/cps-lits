<?php
/*
* Template Name: Page Template
*/

get_header(); ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main">

                        <?php
                        while ( have_posts() ) : the_post();
                        //get_template_part( 'template-parts/content', 'page' );
                        ?>
                            
                            <article id="post-<?php the_ID(); ?>" <?php post_class( '' ); ?>>

                                <?php if ( is_sticky() ) : ?>
                                    <span class="oi oi-bookmark wp-bp-sticky text-muted" title="<?php echo esc_attr__( 'Sticky Post', 'wp-bootstrap-4' ); ?>"></span>
                                <?php endif; ?>
                                <header class="entry-header">
                                    <?php
                                    if ( is_singular() ) :
                                        the_title( '<h1 class="entry-title card-title h2">', '</h1>' );
                                    else :
                                        the_title( '<h2 class="entry-title card-title h3"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" class="text-dark">', '</a></h2>' );
                                    endif;

                                    if ( 'post' === get_post_type() ) : ?>
                                    <!--div class="entry-meta text-muted">
                                        <?php wp_bootstrap_4_posted_on(); ?>
                                    </div--><!-- .entry-meta -->
                                    <?php
                                    endif; ?>
                                </header><!-- .entry-header -->



                                <?php if( is_singular() || get_theme_mod( 'default_blog_display', 'excerpt' ) === 'full' ) : ?>
                                    <div class="entry-content">
                                        <?php
                                            the_content( sprintf(
                                                wp_kses(
                                                    /* translators: %s: Name of current post. Only visible to screen readers */
                                                    __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'wp-bootstrap-4' ),
                                                    array(
                                                        'span' => array(
                                                            'class' => array(),
                                                        ),
                                                    )
                                                ),
                                                get_the_title()
                                            ) );

                                            wp_link_pages( array(
                                                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wp-bootstrap-4' ),
                                                'after'  => '</div>',
                                            ) );
                                        ?>
                                    </div><!-- .entry-content -->
                                <?php else : ?>
                                    <div class="entry-summary">
                                        <?php the_excerpt(); ?>
                                        <div class="">
                                            <a href="<?php echo esc_url( get_permalink() ); ?>" class="btn btn-primary btn-sm"><?php esc_html_e( 'Continue Reading', 'wp-bootstrap-4' ); ?> <small class="oi oi-chevron-right ml-1"></small></a>
                                        </div>
                                    </div><!-- .entry-summary -->
                                <?php endif; ?>


                                <?php if ( 'post' === get_post_type() ) : ?>
                                <footer class="entry-footer card-footer text-muted">
                                    <?php wp_bootstrap_4_entry_footer(); ?>
                                </footer><!-- .entry-footer -->
                                <?php endif; ?>

                                </article><!-- #post-<?php the_ID(); ?> -->		

                        <?php    
                        endwhile; // End of the loop.
                        ?>

                    </main><!-- #main -->
                </div><!-- #primary -->
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

<?php
get_footer();
