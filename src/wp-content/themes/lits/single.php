<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_4
 */

get_header(); ?>


<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <div class="container-fluid">
            <div class="row">
                <div class="col-12 wp-bp-content-width">

				<?php if ( in_category('services') ) { ?>

					<p class="btn-back-to">
						<a href="/category/services">Services</a>
					</p>

					<div class="col-12 col-md-8 location-details">

						<h1 class="heading-1"><?php the_title() ?></h1>

						<div class="location__address">
							<?php if( get_field('street_address') ): trim(the_field('street_address')); endif; ?><?php if( get_field('street_address') && get_field('city') ): ?>, <?php endif; ?>  
							<?php if( get_field('city') ): the_field('city'); endif; ?>                                        
							<!--span class="location__distance">(2.4 km away)</span-->
						</div> <!--/.location__address-->

						<?php if( get_field('hours') ): ?>                               
							<div class="location__hours">
								<strong>Open:</strong> <?php the_field('hours'); ?>
							</div>
						<?php endif; ?> 

						<div class="location__additional-info">
							<?php if( get_field('email') ): ?>                               
								Contat Email: <?php the_field('email'); ?><br>
							<?php endif; ?> 
							<?php if( get_field('website_url') ): ?>                               
								<a href="<?php the_field('website_url'); ?>" target="_blank"><?php the_field('website_url'); ?></a><br>
							<?php endif; ?> 
							<?php if( get_field('program_provided_by') ): ?>                               
								Program provided by:  <?php the_field('program_provided_by'); ?><br>
							<?php endif; ?> 							
						</div>

						<div class="location__cta">
							<?php if( get_field('phone') ): ?>  
								<?php $phone_number = preg_replace("/[^0-9]/", "", get_field('phone')); ?>
								<a href="tel:<?php echo $phone_number; ?>" class="btn btn-primary btn-sm">Call Now</a>							
							<?php endif; ?> 
							
							<?php 
							$location = get_field('location');
							if( !empty($location) ):
							?>
								<a href="https://www.google.com/maps/search/?api=1&query=<?php echo $location['lat']; ?>,<?php echo $location['lng']; ?>" target="_blank" rel="noreferrer" class="btn btn-primary btn-sm">Directions</a>
							<?php endif; ?>   
							        
						</div> <!--/.location__cta-->
						<?php if( get_field('phone') ): ?> <span class="desktop-call-now"><?php echo get_field('phone'); ?></span><?php endif; ?> 
						<hr>		
						<h3 class="subheading"></h3>    
						<?php 
						if ( have_posts() ) : while ( have_posts() ) : the_post();
							the_content();
							endwhile;
						else: ?>
							<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
						<?php endif; ?>
										
						<!--				
							<p>
							Street Address:  
							<?php if( get_field('street_address') ): ?>
								<?php the_field('street_address'); ?><br>
							<?php else: ?>	
								No address given.<br>
							<?php endif; ?>
							City: <?php the_field('city'); ?><br>
							Province: <?php the_field('province'); ?><br>
							Hours: <?php the_field('hours'); ?><br>
							"Call Now" button: <?php the_field('phone'); ?><br>
							"Directions" button: <?php the_field('directions'); ?><br> 

							<?php 
							$location = get_field('location');
							if( !empty($location) ):
							?>
							Lat: <?php echo $location['lat']; ?><br>
							Long: <?php echo $location['lng']; ?><br>
							<?php endif; ?>

							Email: <?php the_field('email'); ?><br>
							Website: <a href="<?php the_field('website_url'); ?>" target="_blank"><?php the_field('website_url'); ?></a><br>
							Program provided by:  <?php the_field('program_provided_by'); ?><br>
							</p>
						//-->

						<?php } elseif ( in_category('questions') ) {?>
						
							<p class="btn-back-to">
								<a href="/category/questions">Questions & Education</a>
							</p>

							<div class="questions-post-page">
								<?php
									while ( have_posts() ) : the_post();
								?>

								<?php								
								if ( has_post_thumbnail() ) {
									wp_bootstrap_4_post_thumbnail();
								} else { ?>
									<img src="https://connectline.ca/wp-content/uploads/2019/02/banner-green.jpg?189db0&amp;189db0" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="https://connectline.ca/wp-content/uploads/2019/02/banner-green.jpg 1400w, https://connectline.ca/wp-content/uploads/2019/02/banner-green-300x171.jpg 300w, https://connectline.ca/wp-content/uploads/2019/02/banner-green-768x439.jpg 768w, https://connectline.ca/wp-content/uploads/2019/02/banner-green-1024x585.jpg 1024w" sizes="(max-width: 1400px) 100vw, 1400px" width="1400" height="800">
								<?php } ?>								
			
								<article id="post-<?php the_ID(); ?>" <?php post_class( '' ); ?>>

										<?php if ( is_sticky() ) : ?>
											<span class="oi oi-bookmark wp-bp-sticky text-muted" title="<?php echo esc_attr__( 'Sticky Post', 'wp-bootstrap-4' ); ?>"></span>
										<?php endif; ?>
										<header class="entry-header">
											<?php
											if ( is_singular() ) :
												the_title( '<h1 class="entry-title card-title h2">', '</h1>' );
											else :
												the_title( '<h2 class="entry-title card-title h3"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" class="text-dark">', '</a></h2>' );
											endif;

											if ( 'post' === get_post_type() ) : ?>
											<!--div class="entry-meta text-muted">
												<?php wp_bootstrap_4_posted_on(); ?>
											</div--><!-- .entry-meta -->
											<?php
											endif; ?>
										</header><!-- .entry-header -->

										

										<?php if( is_singular() || get_theme_mod( 'default_blog_display', 'excerpt' ) === 'full' ) : ?>
											<div class="entry-content">
												<?php
													the_content( sprintf(
														wp_kses(
															/* translators: %s: Name of current post. Only visible to screen readers */
															__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'wp-bootstrap-4' ),
															array(
																'span' => array(
																	'class' => array(),
																),
															)
														),
														get_the_title()
													) );

													wp_link_pages( array(
														'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wp-bootstrap-4' ),
														'after'  => '</div>',
													) );
												?>
											</div><!-- .entry-content -->
										<?php else : ?>
											<div class="entry-summary">
												<?php the_excerpt(); ?>
												<div class="">
													<a href="<?php echo esc_url( get_permalink() ); ?>" class="btn btn-primary btn-sm"><?php esc_html_e( 'Continue Reading', 'wp-bootstrap-4' ); ?> <small class="oi oi-chevron-right ml-1"></small></a>
												</div>
											</div><!-- .entry-summary -->
										<?php endif; ?>


									<?php if ( 'post' === get_post_type() ) : ?>
										<footer class="entry-footer card-footer text-muted">
											<?php wp_bootstrap_4_entry_footer(); ?>
										</footer><!-- .entry-footer -->
									<?php endif; ?>

								</article><!-- #post-<?php the_ID(); ?> -->								


								<?php
									endwhile; // End of the loop.
								?>
							</div>

						<?php } else { ?>

						<?php
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/content', get_post_type() );

						endwhile; // End of the loop.
						?>

						<?php } ?>						
					</div>
				</div> <!-- /.col-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->

    </main> <!--#main-->
</div> <!--/#primary-->   

<?php
get_footer();
