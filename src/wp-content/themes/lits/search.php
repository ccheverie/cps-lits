<?php
/**
 * The template for displaying results from search box
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WP_Bootstrap_4
 */

get_header(); ?>


<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <div class="container">
            <div class="row">
                <div class="col-12 wp-bp-content-width">
                    
            <?php if (cat_is_ancestor_of(38, $cat) or is_category(38)) { ?>
                    
                <h1 class="heading-1">Questions & Education</h1>
                    
                <div id="services-filter" class="form-row align-items-center">
                    <div class="col-auto">
                        <label for="cat">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-filter.png" alt=""/>Filter -
                        </label>
                    </div>
                    <div class="col">
                        <?php 
                            $args = array(
                                'order'              => 'ASC',
                                'orderby'            => 'name',
                                'hide_empty'         => 0,
                                'child_of' => '38',
                                'show_option_all'    => 'All Questions'
                            );
                            wp_dropdown_categories($args); 
                        ?>

                        <script type="text/javascript">
                        var dropdown = document.getElementById("cat");
                        function onCatChange() {
                            if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
                                location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
                            } else {
                                location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat=38";    
                            }
                        }
                        dropdown.onchange = onCatChange;
                        </script>
                    </div> <!--/.col-->
                </div>   <!--/#services-filter--> 

                <?php
                // Sorted alphabetically
                query_posts($query_string . '&orderby=title&order=ASC');
                if ( have_posts() ) :
                echo '<div class="container" id="questions-results-container"><div class="row">';    
                /* Start the Loop */
                while ( have_posts() ) : the_post();
                ?>
                    <div class="col-12 col-md-4">
                    <a href="<?php echo esc_url( get_permalink() ); ?>" class="card" style="background-image: url('<?php the_post_thumbnail_url('large'); ?>');">
                        <div class="card-body text-white">
                            <h5 class="card-title">                          
                                <?php $categories = get_the_category();
                                if ( ! empty( $categories ) ) {
                                    //echo esc_html( $categories[1]->name );   
                                } 
                                $postcat = get_the_category( $post->ID );
                                if ( ! empty( $postcat ) ) {
                                    echo esc_html( $postcat[0]->name );   
                                }                                
                                ?>
                            </h5>
                            <p class="card-text"><?php the_title() ?></p>
                        </div>
                    </a> <!--/.card-->
                    </div>
                <?php
				endwhile;
                echo '</div></div>';
                the_posts_navigation( array(
                    'next_text' => esc_html__( 'Newer Posts', 'wp-bootstrap-4' ),
                    'prev_text' => esc_html__( 'Older Posts', 'wp-bootstrap-4' ),
                ));

                else :                     
                    echo '<p>Sorry, there are currently no results for this category.</p>';
                endif;     
                ?>

            <?php } else { ?>

                <h1 class="heading-1">Services</h1>

                <div id="services-filter" class="form-row align-items-center">
                    <div class="col-auto">
                        <label for="cat">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-filter.png" alt=""/>Filter -
                        </label>
                    </div>
                    <div class="col">
                        <?php  // Excluding these categories in dropdown: Uncategorized (1), Questions (38), Services (39)
                            $args = array(
                                'order'              => 'ASC',
                                'orderby'            => 'name',
                                'hide_empty'         => 0,
                                'child_of'           => '39',
                                'show_option_all'    => 'All Services'
                            );
                            wp_dropdown_categories($args); 
                        ?>

                        <script type="text/javascript">
                        var dropdown = document.getElementById("cat");
                        function onCatChange() {
                            if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
                                location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
                            } else {
                                location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat=39";
                            }
                        }
                        dropdown.onchange = onCatChange;
                        </script>
                    </div> <!--/.col-->
                </div>   <!--/#services-filter--> 

                <?php
                // Sorted alphabetically
                query_posts($query_string . '&orderby=title&order=ASC');
                if ( have_posts() ) :

                /* Start the Loop */
                while ( have_posts() ) : the_post();
                ?>
                    <div id="location-<?php the_ID(); ?>" class="row no-gutters location">
                        <div class="col-8 col-md-9">
                            <a class="location__details" href="<?php echo esc_url( get_permalink() ); ?>">
                                <h2 class="location__name"><?php the_title() ?></h2>
                                <p class="location__subtitle"><?php if( get_field('sub_title') ): the_field('sub_title'); endif; ?></p>
                                <div class="location__address">
                                    <?php if( get_field('street_address') ): trim(the_field('street_address')); endif; ?><?php if( get_field('street_address') && get_field('city') ): ?>, <?php endif; ?>  
                                    <?php if( get_field('city') ): the_field('city'); endif; ?>                                        
                                    <!--<span class="location__distance">(2.4 km away)</span>-->
                                </div>

                            <?php if( get_field('hours') ): ?>                               
                                <div class="location__hours">
                                    <strong>Open:</strong> <?php the_field('hours'); ?>
                                </div>
                            <?php endif; ?> 
                            </a>
                        </div>
                        <div class="col-4 col-md-3">

                        <?php if( get_field('phone') ): ?>  
                            <?php $phone_number = preg_replace("/[^0-9]/", "", get_field('phone')); ?>
							<a href="tel:<?php echo $phone_number; ?>" class="btn btn-primary btn-sm">Call Now</a>                            
                        <?php endif; ?> 
                            
                        <?php 
                        $location = get_field('location');
                        if( !empty($location) ):
                        ?>
                            <a href="https://www.google.com/maps/search/?api=1&query=<?php echo $location['lat']; ?>,<?php echo $location['lng']; ?>" target="_blank" rel="noreferrer" class="btn btn-primary btn-sm">Directions</a>
                        <?php endif; ?> 

                        </div>
                    </div> <!--/.location-<?php the_ID(); ?>--> 

                <?php
				endwhile;

                the_posts_navigation( array(
                    'next_text' => esc_html__( 'Newer Posts', 'wp-bootstrap-4' ),
                    'prev_text' => esc_html__( 'Older Posts', 'wp-bootstrap-4' ),
                ));

                else :                     
                    echo '<p>Sorry, there are currently no results for this Service.</p>';
                endif;     
                ?>
                
            <?php } // end if/else ?> 

                </div> <!-- /.col-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->

    </main> <!--#main-->
</div> <!--/#primary-->   

<?php
get_footer();
