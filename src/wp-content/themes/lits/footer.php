<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_4
 */

?>

	</div><!-- #content -->
	<footer>				
		<div id="footer-navigation" class="container">
			<div class="row">
				<?php wp_nav_menu( array( 'menu' => 'navigation-menu', 'menu_class' => 'navigation-menu'  ) ); ?>				
			</div> <!--/.row-->
		</div> <!--/.container-->

	</footer>
</div><!-- #page -->
	
<?php wp_footer(); ?>

</body>
</html>
