<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cps-lits');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Kk,#;T>4C%M {_bNkS}6zkJ]~}^rNrl}z+JkX+mhy!R&k{g>a@m|/.]*lr6Etz@/');
define('SECURE_AUTH_KEY',  ':F2gJ)hQGnyM74j#^TD7e%}^DrPhBFAtjIcQ!qt6B?X<<EK(<8by<ix=miRu^<mH');
define('LOGGED_IN_KEY',    'ihn2&pytO]xR}YoN5eJy/x*OO6a1<fVeI2~i_$ti9|=2&%uci<RHtI^UlGL~{Pf5');
define('NONCE_KEY',        ':rqiV<,heOJu [[s2e#N*8W<b|bfPNWI%9n3:{?OD>%APHEVk<a%3G |-w0G*_#.');
define('AUTH_SALT',        'mC{)kW|Kh}XQxw1pK&@Bwg-}v]qB~g/+uidT8@vs5bTuMS~lB,ChYzB6<E3<ym?:');
define('SECURE_AUTH_SALT', 'aD!LoL0jeGAR0Kv,aJp9#tk[]NE?]F7=GPL_!vWEpp]j/*Wg rF7q&WOe2kih}Yq');
define('LOGGED_IN_SALT',   'nz?]9!JB.,L%C3p9j7+1cVtb)H9.Ji&><v*5:_#5+EjOK8>,B]eUiV+s{nSO(Z5}');
define('NONCE_SALT',       '<>ld%bw#mwpf}YTV*@$8*W!8:38yP:h!,TN=CzTjyY$1#@nPG#M9#c3qA-o22Y>b');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'lits_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

//if (!session_id())
//	session_start();
	
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
